README
======
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Lithuanian, edition 1.3. See the wiki pages of the [PARSEME corpora initiative](https://gitlab.com/parseme/corpora/-/wikis/home) for the full documentation of the annotation principles.

The present Lithuanian data result from an enhancement of the Lithuanian part of the [PARSEME corpus v 1.1](http://hdl.handle.net/11372/LRT-2842).
For the changes with respect to version 1.1, see the change log below.

Corpora
-------
All annotated data come from one source:
DELFI - Lithuanian news portal http://www.delfi.lt/. Texts are published during one month period (2016-08-01 - 2016-09-01) and belong to 9 topics: car review, lifestyle, science, people, news, projects, sport, business, various.
Original article texts are merged into one file. 

Total number of sentences: 11104.


Provided annotations
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Here is detailed information about some columns:

<!--
* LEMMA (column 3): Available. Automatically annotated.
* UPOS (column 4): Available. Automatically annotated.
* FEATS (column 6): Available. Automatically annotated.
* MISC (column 10): No-space information available. Automatically annotated.
* LEMMA (column 3): Available. Manually annotated.
-->
* UPOS and XPOS (columns 4 and 5): Available. Automatically generated (UDPipe 2, model lithuanian-alksnis-ud-2.10-220711). The tagset is the one of [UD POS-tags](http://universaldependencies.org/u/pos).
* FEATS (column 6): Available. Automatically generated (UDPipe 2, model lithuanian-alksnis-ud-2.10-220711). The tagset is [UD features](http://universaldependencies.org/u/feat/index.html).
* HEAD and DEPREL (columns 7 and 8): Available. Automatically generated (UDPipe 2, model lithuanian-alksnis-ud-2.10-220711). The tagset is [UD dependency relations](http://universaldependencies.org/u/dep).
* MISC (column 10): No-space information available. Automatically generated (UDPipe 2, model lithuanian-alksnis-ud-2.10-220711).

* PARSEME:MWE (column 11): Manually annotated. The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.3/?page=030_Categories_of_VMWEs) are annotated: VID, LVC.full, LVC.cause.

VMWEs in this language for version 1.0 have been annotated by two annotators, and for version 1.1 they have been reannotated by a single annotator. IN version 1.3 no modifications were done in the PARSEME:MWE column.


Tokenization
------------
* URLs: are not recognized and might be split in parts.
* Numbers: float numbers are preserved as single tokens, unless there are spaces in the middle of the number.
* Abbreviations: dots are tokenized apart from words, e.g., prof. is tokenized as two tokens "prof" and ".".
* Each orthographic word separated by spaces is considered as a single token.
* Hybrid numerical abbreviations (with the number in digits and Lithuanian endings), e.g., 15-os (penkiolikos), 6-ąją (šeštąją), which are divided in three tokens (e.g., 15, -, os) by tokenizer, were manually corrected as one token. 
* Word forms with apostroph, e.g., men's, Eugene'as, which are divided in three tokens (e.g., Eugene, -, as) by tokenizer, were manually corrected as one token.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 


Authors
----------
The VMWEs annotations were performed by Jolanta Kovalevskaitė, Erika Rimkutė.
The corpus data were prepared by Loic Boizou, Ieva Bumbulienė.


License
----------
The VMWE data are distributed under the terms of the [CC-BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.


Contact
----------
* Jolanta Kovalevskaitė: jolanta.kovalevskaite@vdu.lt
* Loic Boizou: lboizou@gmail.com

Future work
----------
In version 1.1 of the corpus contained automatic annotation performed using `Semantika.lt` web service and converted to UD tagset.
MWEs of grammatical nature (multi-word pronouns, multi-word adverbs, multi-word conjunctions, multi-word-particles) which were treated as one token (with space inside) were split into parts: the MWE part of speech was assigned to each part of MWE, grammatical information was assigned to the first part only, the XPOS field was assigned the value SEQ for each non-initial part.

These data were replaced by those stemming from UDPipe When morphosyntax was re-annotated with UDPipe 2 (model lithuanian-alksnis-ud-2.10-220711) for version 1.3. 
In the future it might be interesting to restore the updated `Semantika.lt` annotations.

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - The morphosyntactic annotation (columns UPOS to MISC) was re-generated automatically with [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2/) (lithuanian-alksnis-ud-2.10-220711 model) by Agata Savary. This suppressed the previous XPOS data stremming from `Semantika.lt` and containing data on non-verbal VMEs.  
- **2018-04-30**:
  - One article was removed in version 1.1 corpus because of incorrect hyphenation.
  - Several sentences of another article were removed because of incorrect character encoding.
  - [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT.
- **2017-01-20**:
  - [Version 1.0](http://hdl.handle.net/11372/LRT-2282) of the corpus was released on LINDAT.   
